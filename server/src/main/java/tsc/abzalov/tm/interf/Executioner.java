package tsc.abzalov.tm.interf;

@FunctionalInterface
public interface Executioner {

    void execute();

}
