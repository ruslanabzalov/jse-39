package tsc.abzalov.tm.model;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Data
public abstract class AbstractEntity implements Serializable {

    @NotNull
    private Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;

}
