package tsc.abzalov.tm.component;

import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public abstract class AbstractBackgroundTaskComponent {

    protected static final int INITIAL_DELAY = 1;

    protected static final int BACKUP_DELAY = 30;

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final IEndpointLocator endpointLocator;

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final ScheduledExecutorService scheduledExecutorService;

    public AbstractBackgroundTaskComponent(@NotNull IEndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public abstract void run();

}
