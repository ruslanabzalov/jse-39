package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public boolean hasData(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
            return projectRepository.projectsSizeByUserId(userId) != 0 && taskRepository.tasksSizeByUserId(userId) != 0;
        }
    }

    @Override
    @SneakyThrows
    public void addTaskToProjectById(@Nullable Long taskId, @Nullable Long projectId) {
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskId = Optional.ofNullable(taskId).orElseThrow(EmptyIdException::new);

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
            taskRepository.addTaskToProjectById(taskId, projectId);
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public Project findProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable val searchedProject = projectRepository.findProjectById(id);
            return Optional.ofNullable(searchedProject).orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public Task findTaskById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable val searchedTask = taskRepository.findTaskById(id);
            return Optional.ofNullable(searchedTask).orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
            return Optional
                    .ofNullable(taskRepository.findProjectTasksById(userId, projectId))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @SneakyThrows
    public void deleteProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
            projectRepository.removeProjectById(id);
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
            taskRepository.deleteProjectTasksById(userId, projectId);
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

}
