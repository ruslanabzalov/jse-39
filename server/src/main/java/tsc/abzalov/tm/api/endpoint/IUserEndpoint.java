package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;

public interface IUserEndpoint {

    @Nullable
    User findUserById(@Nullable Session session);

    @Nullable
    User editPasswordById(@Nullable Session session, @Nullable String newPassword);

    @Nullable
    User editUserInfoById(@Nullable Session session, @Nullable String firstName, @Nullable String lastName);

}
