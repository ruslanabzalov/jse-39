package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void command(@Nullable String cmd);

    void error(@NotNull Exception exception);

}
