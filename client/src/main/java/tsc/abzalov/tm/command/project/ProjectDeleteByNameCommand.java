package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;


public final class ProjectDeleteByNameCommand extends AbstractCommand {

    public ProjectDeleteByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "delete-project-by-name";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Delete project by name.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE PROJECT BY NAME");
        @NotNull val projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areEntitiesExist = projectEndpoint.projectsSize(session) != 0;

        if (areEntitiesExist) {
            projectEndpoint.removeProjectByName(session, inputName());
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Projects are empty.\n");
    }

}
