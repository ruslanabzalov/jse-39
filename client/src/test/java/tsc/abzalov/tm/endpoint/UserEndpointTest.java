package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;

import static org.junit.jupiter.api.Assertions.*;

class UserEndpointTest {

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private static final String FIRSTNAME = "Admin";

    @NotNull
    private static final String LASTNAME = "Admin";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private Session session;

    @BeforeEach
    void setUp() {
        session = sessionEndpoint.openSession(LOGIN, PASSWORD);
    }

    @AfterEach
    void tearDown() {
        if (session != null) sessionEndpoint.closeSession(session);
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find User By Id Test")
    void findUserById() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> userEndpoint.findUserById(null)
                ),
                () -> {
                    @Nullable User user = userEndpoint.findUserById(session);
                    assertNotNull(user);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Password By Ud Test")
    void editPasswordById() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> userEndpoint.editPasswordById(null, PASSWORD + PASSWORD)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> userEndpoint.editPasswordById(session, null)
                ),
                () -> {
                    @Nullable val editedUser = userEndpoint.editPasswordById(session, PASSWORD + PASSWORD);
                    assertNotNull(editedUser);
                    userEndpoint.editPasswordById(session, PASSWORD);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit User Info By Id Test")
    void editUserInfoById() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> userEndpoint.editUserInfoById(null, FIRSTNAME, LASTNAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> userEndpoint.editUserInfoById(this.session, null, LASTNAME)
                ),
                () -> {
                    @NotNull val user = userEndpoint.editUserInfoById(session, FIRSTNAME + FIRSTNAME, null);
                    assertNotNull(user);
                    assertEquals(FIRSTNAME + FIRSTNAME, user.getFirstName());
                    assertNull(user.getLastName());
                    userEndpoint.editUserInfoById(session, FIRSTNAME, LASTNAME);
                }
        );
    }

}